# [Europe Weather App](https://bitbucket.org/cetinaziz/weather-app/src/main/README.md)

 ![version](https://img.shields.io/badge/version-1.0.0-blue.svg)  ![license](https://img.shields.io/badge/license-MIT-blue.svg)

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.0.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.


## Table of Contents

* [Pages](#Pages)
* [Quick Start](#quick-start)
* [Documentation](#documentation)
* [File Structure](#file-structure)
* [Resources](#resources)
* [Useful Links](#useful-links)


## Pages

| Weather Page | City Detail Page
| --- | --- |
| [![Weather Page](https://bitbucket.org/cetinaziz/weather-app/raw/cadf94cf3d4b0b2493d3433635cdd647393e50ed/src/assets/img/app-screenshot.png)](https://bitbucket.org/cetinaziz/weather-app/raw/cadf94cf3d4b0b2493d3433635cdd647393e50ed/src/assets/img/app-screenshot.png)  | [![City Detail Page](https://bitbucket.org/cetinaziz/weather-app/raw/cadf94cf3d4b0b2493d3433635cdd647393e50ed/src/assets/img/app-screnshot-1.png  )](https://bitbucket.org/cetinaziz/weather-app/raw/cadf94cf3d4b0b2493d3433635cdd647393e50ed/src/assets/img/app-screnshot-1.png  ) 


## Quick start

- Clone the repo: `git clone https://bitbucket.org/cetinaziz/weather-app.git`.
- Follow the steps.

## Documentation

This project consists of components as can be seen in the file structure section below. 
It has also been strengthened with the weather service to provide data flow and the other functionality to these components. 
In the weather service you can also see the API calls from [http://api.openweathermap.org](http://api.openweathermap.org) and other functions.
In models folder you can see the City model. 
Various mock data has been created to provide the data source within the data folder for components. 
And also routes are in the routes.ts file in data folder.
[ngx-spinner](https://www.npmjs.com/package/ngx-spinner) library is used for loader.
[Bootstrap5](https://www.npmjs.com/package/bootstrap) is used for front-end framework 
[FontAwesome](https://www.npmjs.com/package/font-awesome) is used for icons.


## File Structure
You'll find the following directories and files in file structure:

```
weather-app
├── src
│   ├── app
│   │   ├── components
│   │   │   ├── city
│   │   │   │   ├── city.component.css
│   │   │   │   ├── city.component.html
│   │   │   │   ├── city.component.spec.ts
│   │   │   │   └── city.component.ts
│   │   │   ├── city-detail
│   │   │   │   ├── city-detail.component.css
│   │   │   │   ├── city-detail.component.html
│   │   │   │   ├── city-detail.component.spec.ts
│   │   │   │   └── city-detail.component.ts
│   │   │   ├── city-hourly-detail
│   │   │   │   ├── city-hourly-detail.component.css
│   │   │   │   ├── city-hourly-detail.component.html
│   │   │   │   ├── city-hourly-detail.component.spec.ts
│   │   │   │   └── city-hourly-detail.component.ts
│   │   │   ├── header
│   │   │   │   ├── header.component.css
│   │   │   │   ├── header.component.html
│   │   │   │   ├── header.component.spec.ts
│   │   │   │   └── header.component.ts
│   │   │   ├── spinner
│   │   │   │   ├── spinner.component.css
│   │   │   │   ├── spinner.component.html
│   │   │   │   ├── spinner.component.spec.ts
│   │   │   │   └── spinner.component.ts
│   │   │   └── weather
│   │   │       ├── weather.component.html
│   │   │       ├── weather.component.scss
│   │   │       ├── weather.component.spec.ts
│   │   │       └── weather.component.ts
│   │   ├── data
│   │   │   ├── dummy-city-hourly-info.ts
│   │   │   ├── dummy-city.ts
│   │   │   ├── mock-cities.ts
│   │   │   └── routes.ts
│   │   ├── models
│   │   │   └── City.ts
│   │   ├── services
│   │   │   ├── weather.service.spec.ts
│   │   │   └── weather.service.ts
│   │   ├── app-routing.module.ts
│   │   ├── app.component.css
│   │   ├── app.component.html
│   │   ├── app.component.spec.ts
│   │   ├── app.component.ts
│   │   └── app.module.ts
│   ├── assets
│   │   ├── img
│   │   └── .gitkeep
│   ├── environments
│   │   ├── environment.prod.ts
│   │   └── environment.ts
│   ├── favicon.ico
│   ├── index.html
│   ├── main.ts
│   ├── polyfills.ts
│   ├── styles.css
│   └── test.ts
├── .browserslistrc
├── .editorconfig
├── .gitignore
├── angular.json
├── karma.conf.js
├── package-lock.json
├── package.json
├── README.md
├── tsconfig.app.json 
├── tsconfig.json  
└── tsconfig.spec.json

```

## Resources
- BitBucket: <https://bitbucket.org/cetinaziz/weather-app/src/main/>
- Documentation: <https://bitbucket.org/cetinaziz/weather-app/src/main/README.md>

## Useful Links

- [Angular Products](https://angular.io/)
- [Open Weather Map](https://openweathermap.org/api)
- [Fontawesome](https://fontawesome.com/)
- [Bootstrap](https://getbootstrap.com/)

### Author Social Media

LinkedIn: <https://www.linkedin.com/in/azizcetin/>

Website: <https://www.azizcetin.com>
